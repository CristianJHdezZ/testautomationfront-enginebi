package net.enginebi.certification.front.stepsdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import net.enginebi.certification.front.tasks.OpenThe;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;


import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class GeneralSteps {

    @Before
    public void setUp() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("The user is on the login page")
    public void theUserIsOnTheLoginPage() {
        theActorCalled("UserTester").wasAbleTo(OpenThe.page());
    }

    @After
    public void AfterSteps() {
        getDriver().quit();
    }
}
