package net.enginebi.certification.front.stepsdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.enginebi.certification.front.questions.VerifyCurrentUrl;
import net.enginebi.certification.front.questions.VerifyMessage;
import net.enginebi.certification.front.tasks.Login;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.thucydides.core.util.EnvironmentVariables;


import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class CheckValidCredentialsSteps {
    EnvironmentVariables environmentVariables = null;

    @When("The user enters valid credentials and clicks on the login button")
    public void theUserEntersValidCredentialsAndClicksOnTheLoginButton() {
        theActorInTheSpotlight().attemptsTo(Login.inThePager(environmentVariables.optionalProperty("ValidCredentials.user").get(),environmentVariables.optionalProperty("ValidCredentials.password").get()));
    }
    @Then("The user should be redirected to the URL {string}")
    public void theUserShouldBeRedirectedToTheUrl(String extUrl) {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(VerifyCurrentUrl.WithValue(extUrl)));

    }
    @Then("A welcome message {string} must be shown")
    public void aWelcomeMessageMustBeShown(String strExpectedMessage) {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(VerifyMessage.withValue(strExpectedMessage)));
    }
}
