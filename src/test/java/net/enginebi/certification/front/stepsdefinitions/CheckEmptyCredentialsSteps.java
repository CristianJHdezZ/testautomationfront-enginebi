package net.enginebi.certification.front.stepsdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.enginebi.certification.front.questions.VerifyMessageErrorLogin;
import net.enginebi.certification.front.tasks.Login;
import net.serenitybdd.screenplay.GivenWhenThen;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class CheckEmptyCredentialsSteps {

    @When("the user leaves the username field and password field empty and clicks the login button")
    public void theUserLeavesTheUsernameFieldAndPasswordFieldEmptyAndClicksTheLoginButton() {
        theActorInTheSpotlight().attemptsTo(Login.inThePager("",""));
    }

    @Then("the user should see an error message {string}")
    public void theUserShouldSeeAnErrorMessage(String strExpectedMessage) {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(VerifyMessageErrorLogin.withValue(strExpectedMessage)));
    }

    @Then("you should see an error message {string}")
    public void youShouldSeeAnErrorMessage(String strExpectedMessage) {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(VerifyMessageErrorLogin.withValue(strExpectedMessage)));
    }
}
