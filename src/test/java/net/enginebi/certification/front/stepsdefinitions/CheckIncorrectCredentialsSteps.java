package net.enginebi.certification.front.stepsdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.enginebi.certification.front.questions.VerifyMessageErrorLogin;
import net.enginebi.certification.front.tasks.Login;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.thucydides.core.util.EnvironmentVariables;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class CheckIncorrectCredentialsSteps {

    EnvironmentVariables environmentVariables = null;

    @When("The user enters invalid username and password and clicks the login button")
    public void theUserEntersInvalidUsernameAndPasswordAndClicksTheLoginButton() {
        theActorInTheSpotlight().attemptsTo(Login.inThePager(environmentVariables.optionalProperty("IncorrectCredentials.user").get(),environmentVariables.optionalProperty("IncorrectCredentials.password").get()));
    }

    @Then("User should then see an error message {string}")
    public void userShouldThenSeeAnErrorMessage(String strExpectedMessage) {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(VerifyMessageErrorLogin.withValue(strExpectedMessage)));
    }
}
