package net.enginebi.certification.front.stepsdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.enginebi.certification.front.questions.VerifyCurrentPage;
import net.enginebi.certification.front.questions.VerifyCurrentUrl;
import net.enginebi.certification.front.tasks.ClickOn;
import net.serenitybdd.screenplay.GivenWhenThen;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class CheckPageOpensSteps {
    @When("I click on menu item {string}")
    public void iClickOnMenuItem(String strElementMenuItem) {
        theActorInTheSpotlight().attemptsTo(ClickOn.theElement(strElementMenuItem));
    }
    @Then("the page URL should be equal to the following URL {string}")
    public void thePageURLShouldBeEqualToTheFollowingURL(String strExpectedURL) {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(VerifyCurrentUrl.WithValue(strExpectedURL)));

    }
    @Then("the page should contain the following element {string}")
    public void thePageShouldContainTheFollowingElement(String strExpectedLabel) {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(VerifyCurrentPage.withValue(strExpectedLabel)));
    }
}
