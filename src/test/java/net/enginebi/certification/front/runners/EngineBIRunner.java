package net.enginebi.certification.front.runners;


import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/enginebi.feature",
        glue = "net.enginebi.certification.front.stepsdefinitions",
        plugin = {"pretty"},
        tags = "@AllTests",
        snippets = CucumberOptions.SnippetType.CAMELCASE
)
public class EngineBIRunner {
}
