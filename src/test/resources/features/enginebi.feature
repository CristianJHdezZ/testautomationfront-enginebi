@AllTests
Feature: Access features to the EngineBI platform
  As a user of EngineBI
  I want to log in to the platform
  So that I can access my dashboard

  Background: Login to page EngineBI
    Given The user is on the login page


  @CheckValidCredentials
  Scenario: Verify successful login with valid credentials
    When The user enters valid credentials and clicks on the login button
    Then The user should be redirected to the URL "home"
    And A welcome message "Welcome, Cristian" must be shown


  @CheckIncorrectCredentials
  Scenario: Verify login failed with incorrect credentials
    When The user enters invalid username and password and clicks the login button
    Then User should then see an error message "Invalid login attempt."


  @CheckEmptyCredentials
  Scenario: Verify failed login with empty username and password
    When the user leaves the username field and password field empty and clicks the login button
    Then the user should see an error message "The Username field is required."
    And you should see an error message "The Password field is required."
    And you should see an error message "Not valid login model state."


  @CheckPageOpens
  Scenario Outline: Verify that you navigate through each of the menu options and validate that each page opens
    When The user enters valid credentials and clicks on the login button
    And I click on menu item "<element>"
    Then the page URL should be equal to the following URL "<url>"
    And the page should contain the following element "<label>"

    Examples:
      | element              | url                 | label                |
      | Home                 | home                | Welcome, Cristian    |
      | Budget Builder       | budget-planner/list | Budget Builder       |
      | Report Central       | reports/home        | Report Central       |
      | Management           | management/users    | Management Dashboard |



