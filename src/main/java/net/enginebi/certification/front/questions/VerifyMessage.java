package net.enginebi.certification.front.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static net.enginebi.certification.front.userinterface.HomePage.LABEL_WELCOME;

public class VerifyMessage implements Question<Boolean> {
    private String strExpectedMessage;

    public VerifyMessage(String strExpectedMessage) {
        this.strExpectedMessage = strExpectedMessage;
    }

    public static VerifyMessage withValue(String strExpectedMessage) {
        return new VerifyMessage(strExpectedMessage);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        return Text.of(LABEL_WELCOME).answeredBy(actor).equalsIgnoreCase(strExpectedMessage);
    }
}
