package net.enginebi.certification.front.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static net.enginebi.certification.front.userinterface.LoginPage.LABEL_MESSAGE_ERROR;

public class VerifyMessageErrorLogin implements Question<Boolean> {
    private String strExpectedMessage;

    public VerifyMessageErrorLogin(String strExpectedMessage) {
        this.strExpectedMessage = strExpectedMessage;
    }

    public static VerifyMessageErrorLogin withValue(String strExpectedMessage) {
        return new VerifyMessageErrorLogin(strExpectedMessage);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        System.out.println("Label get: "+Text.of(LABEL_MESSAGE_ERROR).answeredBy(actor));
        return Text.of(LABEL_MESSAGE_ERROR).answeredBy(actor).contains(strExpectedMessage);
    }
}
