package net.enginebi.certification.front.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class VerifyCurrentUrl implements Question<Boolean> {
    private String strUrl;

    public VerifyCurrentUrl(String strUrl) {
        this.strUrl = strUrl;
    }

    public static VerifyCurrentUrl WithValue(String strUrl) {
        return new VerifyCurrentUrl(strUrl);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        System.out.println("Current Url: "+getDriver().getCurrentUrl());
        return getDriver().getCurrentUrl().contains(strUrl);
    }
}
