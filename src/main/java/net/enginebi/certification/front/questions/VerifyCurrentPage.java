package net.enginebi.certification.front.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static net.enginebi.certification.front.userinterface.HomePage.LABEL_PAGE_MODULE;

public class VerifyCurrentPage implements Question<Boolean> {
    private String strExpectedLabel;

    public VerifyCurrentPage(String strExpectedLabel) {
        this.strExpectedLabel = strExpectedLabel;
    }


    public static VerifyCurrentPage withValue(String strExpectedLabel) {
        return new VerifyCurrentPage(strExpectedLabel);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        System.out.println("Label Page Get: "+Text.of(LABEL_PAGE_MODULE).answeredBy(actor));
        return Text.of(LABEL_PAGE_MODULE).answeredBy(actor).equalsIgnoreCase(strExpectedLabel);
    }
}
