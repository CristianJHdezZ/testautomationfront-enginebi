package net.enginebi.certification.front.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static net.enginebi.certification.front.userinterface.LoginPage.*;

public class Login implements Task {
    private String strUser;
    private String strPassword;

    public Login(String strUser, String strPassword) {
        this.strUser = strUser;
        this.strPassword = strPassword;
    }

    public static Login inThePager(String strUser, String strPassword) {
        return Tasks.instrumented(Login.class,strUser,strPassword);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        System.out.println("Credenciales: "+strUser+"  -  ********");
        actor.attemptsTo(
                Enter.keyValues(strUser).into(INPUT_USER),
                Enter.keyValues(strPassword).into(INPUT_PASSWORD),
                Click.on(BUTTON_LOGIN)
        );
    }
}
