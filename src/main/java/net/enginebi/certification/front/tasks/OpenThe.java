package net.enginebi.certification.front.tasks;


import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;


public class OpenThe implements Task {
    public static OpenThe page() {
        return Tasks.instrumented(OpenThe.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.wasAbleTo(Open.browserOn().thePageNamed("login.page"));
        System.out.println("url: "+getDriver().getCurrentUrl());
    }
}
