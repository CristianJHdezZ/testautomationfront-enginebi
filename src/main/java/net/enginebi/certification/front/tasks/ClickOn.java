package net.enginebi.certification.front.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

import static net.enginebi.certification.front.userinterface.HomePage.ITEM_MENU_HOME;


public class ClickOn implements Task {
    private String strElementMenuItem;

    public ClickOn(String strElementMenuItem) {
        this.strElementMenuItem = strElementMenuItem;
    }

    public static ClickOn theElement(String strElementMenuItem) {
        return Tasks.instrumented(ClickOn.class,strElementMenuItem);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(ITEM_MENU_HOME.of(strElementMenuItem))
        );
    }
}
