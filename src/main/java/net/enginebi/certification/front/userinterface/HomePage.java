package net.enginebi.certification.front.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class HomePage {
    public static final Target LABEL_WELCOME = Target.the("label Welcome").located(By.xpath("//p[@class='home-user']"));
    public static final Target ITEM_MENU_HOME = Target.the("Item Menu Home").locatedBy("//a[@nztooltiptitle='{0}']");
    public static final Target LABEL_PAGE_MODULE = Target.the("Label page module").located(By.xpath("(//div[@class='fixed-container page-content' or @class='fixed-container page-content mb-20 ng-star-inserted']//p)[1]"));


}
