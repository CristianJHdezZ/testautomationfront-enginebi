package net.enginebi.certification.front.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class LoginPage {
    public static final Target INPUT_USER = Target.the("Input User").located(By.id("email-input"));
    public static final Target INPUT_PASSWORD = Target.the("Input Pass").located(By.id("floatingPassword"));
    public static final Target BUTTON_LOGIN = Target.the("BUTTON LOGIN").located(By.id("loginButton"));
    public static final Target LABEL_MESSAGE_ERROR = Target.the("Message Error").located(By.xpath("//div[@class='danger validation-summary-errors']//ul"));

}
