# RetoAutomationQA - EngineBI
#**Test Automation Frontend - Screenplay Pattern - Selenium WebDriver - Cucumber(Gherkin) - SerenityBDD**

## Pre-requisitos
1. Java 8
2. Gradle 5.2.1 o Superior
3. IDE de desarrollo
	- b) IntelliJ
4. Dependencias: se deben definir en el archivo **build.gradle**
	
```java
   ext {
        slf4jVersion = '1.7.32'
        serenityCoreVersion = '3.0.5'
        cucumberCoreVersion = '6.11.0'
        serenityCucumberVersion = '2.6.0'
        junitVersion = '4.13.2'
        assertJVersion = '3.21.0'
        logbackVersion = '1.2.6'
        fakerVersion = '1.0.2'

        }

        dependencies {
        implementation "com.github.javafaker:javafaker:${fakerVersion}",
        "net.serenity-bdd:serenity-ensure:${serenityCoreVersion}",
        "net.serenity-bdd:serenity-screenplay-webdriver:${serenityCoreVersion}",
        "org.slf4j:slf4j-api:${slf4jVersion}",
        "org.slf4j:slf4j-log4j12:${slf4jVersion}"

        testImplementation "net.serenity-bdd:serenity-core:${serenityCoreVersion}",
        "io.cucumber:cucumber-core:${cucumberCoreVersion}",
        "net.serenity-bdd:serenity-cucumber6:${serenityCucumberVersion}",
        "io.cucumber:cucumber-junit:${cucumberCoreVersion}",
        "junit:junit:${junitVersion}",
        "org.assertj:assertj-core:${assertJVersion}"

        testImplementation("io.github.bonigarcia:webdrivermanager:5.6.3")
        }
````

## Descripción

Este es un reto de automatización de pruebas para Frontend, se encuentra creado bajo el patrón de diseño Screenplay e integra varias herramientas que incluyen a SerenityBDD y Cucumber dentro del manejador de proyecto Gradle.

La estructura del proyecto está conformada por paquetes nombrados de la siguiente manera:


![img.png](src/test/resources/images/img.png)

---
- ☕📦️ **exceptions**:
  Clases que controlan las posibles excepciones técnicas y de negocios que se presentan durante la ejecución de pruebas
  
- ☕📦️ **interactions**:
  Clases que representan las interacciones directas con la interfaz de usuario.
  
- ☕📦️ **model**:
  Clases relacionadas con el modelo de dominio.

- ☕📦️ **questions**:
  Objetos usados para consultar acerca del estado de la aplicación o verificar el resultado esperado de la ejecución de prueba.
  
- ☕📦️ **tasks**:
  Clases que representan tareas que realiza el actor en el ámbito de proceso de negocio.

- ☕📦️ **userinterface**: 
  Page Objects y Page Elements. Mapean los objetos de la interfaz de usuario o aplicación Web.
  
- ☕📦️ **util**:
  Clases de utilidad.
  
- ☕📦️ **runners**
  Clases que permiten correr los tests.
  
- ☕📦️ **step definitions**
  Clases que mapean las líneas Gherkin a código java y donde se llaman todas las tareas(tasks) y preguntas(questions) creadas para la ejecución de la prueba.
  
- ☕📦️ **features**:
  La representación de las historias de usuarios en archivos en lenguaje gherkin (.feature).

---
  
**Este proyecto cuenta con los siguientes scenarios:**

  ##Escenario:
   ```gherkin
    @AllTests
Feature: Access features to the EngineBI platform
  As a user of EngineBI
  I want to log in to the platform
  So that I can access my dashboard

  Background: Login to page EngineBI
    Given The user is on the login page


  @CheckValidCredentials
  Scenario: Verify successful login with valid credentials
    When The user enters valid credentials and clicks on the login button
    Then The user should be redirected to the URL "home"
    And A welcome message "Welcome, Cristian" must be shown


  @CheckIncorrectCredentials
  Scenario: Verify login failed with incorrect credentials
    When The user enters invalid username and password and clicks the login button
    Then User should then see an error message "Invalid login attempt."


  @CheckEmptyCredentials
  Scenario: Verify failed login with empty username and password
    When the user leaves the username field and password field empty and clicks the login button
    Then the user should see an error message "The Username field is required."
    And you should see an error message "The Password field is required."
    And you should see an error message "Not valid login model state."


  @CheckPageOpens
  Scenario Outline: Verify that you navigate through each of the menu options and validate that each page opens
    When The user enters valid credentials and clicks on the login button
    And I click on menu item "<element>"
    Then the page URL should be equal to the following URL "<url>"
    And the page should contain the following element "<label>"

    Examples:
      | element              | url                 | label                |
      | Home                 | home                | Welcome, Cristian    |
      | Budget Builder       | budget-planner/list | Budget Builder       |
      | Report Central       | reports/home        | Report Central       |
      | Management           | management/users    | Management Dashboard |
  ```
   
  
---
## Instalación

Para instalar el proyecto deben seguir los siguientes pasos:
  
**1. Clonar el proyecto**

```
git clone https://gitlab.com/CristianJHdezZ/testautomationfront-enginebi.git
```

**2. Importar el proyecto**
  
Para importar el proyecto debe hacerlo desde el IDE de su preferencia seleccionando la carpeta **"RetoEngineBI-AutomationTesting"** como un proyecto Gradle. 
  
  
**3. Ejecutar el proyecto** 

Para ejecutar el proyecto debe hacerlo a través de uno de los siguientes pasos: 
  
**3.1 A través de línea de comando**
  
Si se desea realizar a través de la línea de comando se debe ejecutar desde una consola de comandos (de su preferencia) con el comando: 

```
gradle clean test aggregate --info
```
  
**3.2 A través de IDE** 
  
Si se desea realizar la ejecución a través del IDE de desarrollo, se debe seleccionar la clase **"EngineBIRunner.java"** y dar clic derecho, seleccionar la opción que permita correr la prueba. 
  

  
---
## ️Autores
* **Cristian Hernandez Z.** - [CristianHdezZ](https://github.com/CristianHdezZ/)
---

